"""
Definition of views.
"""

from django.shortcuts import render
from django.http import HttpRequest
from django.template import RequestContext
from datetime import datetime
from django.http import JsonResponse

def contact(request):
    """Renders the contact page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/contact.html',
        {
            'title':'Contact',
            'message':'Your contact page.',
            'year':datetime.now().year,
        }
    )

def home(request):
    """Renders the home page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/index.html',
        {
            'title':'Home Page',
            'year':datetime.now().year,
        }
    )



def about(request):
    """Renders the about page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/about.html',
        {
            'title':'About',
            'message':'Your application description page.',
            'year':datetime.now().year,
        }
    )

def math_calculate(request):
    data = request.GET.get('txt', None)
    result = 0.0;
    mathresult = {'data':result};
    if "+" in data:
        add = data.split('+');
        result = float(add[0]) + float(add[1]);  
        mathresult = {'data':result};
    elif "-" in data:
         subtract = data.split('-');
         result = float(subtract[0]) - float(subtract[1]);  
         mathresult = {'data':result};
    elif "*" in data:
         multiply = data.split('*');
         result = float(multiply[0]) * float(multiply[1]); 
         mathresult = {'data':result};
    elif "/" in data:
         division = data.split('/');
         result = float(division[0]) / float(division[1]);
         mathresult = {'data':result};
    else:
        result = 0.0;
        mathresult = {'data':result};
    return JsonResponse(mathresult)
